package com.example.pamela.geofence;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by pamela on 12/03/2018.
 */

public class ViewReport extends AppCompatActivity {

    DatabaseReference ref;
    LinearLayout linear;

    private ArrayList<NotifyElement> notifyElements= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_report);

        linear = (LinearLayout) findViewById(R.id.linear);
        ref = FirebaseDatabase.getInstance().getReference("notificationList");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    NotifyElement notifyElement = ds.getValue(NotifyElement.class);
                    notifyElements.add(notifyElement);
                }

                addReports();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ERROR", "database error");
            }
        });


    }

    private void addReports() {
        if (notifyElements.size()>0){
            TextView textView = new TextView(this);
            textView.setText("Reports: ");
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
            textView.setTextSize(40);
            linear.addView(textView);

            TextView txGeofence[] =new TextView[notifyElements.size()];
            TextView txMessage[] =new TextView[notifyElements.size()];
            TextView txTime[] =new TextView[notifyElements.size()];

            for (int i =0; i<notifyElements.size(); i++){
                txGeofence[i]= new TextView(this);
                txGeofence[i].setTextSize(18);
                txGeofence[i].setText("Geofence: "+notifyElements.get(i).getGeofence());

                txMessage[i] = new TextView(this);
                txMessage[i].setTextSize(15);
                txMessage[i].setText("Message:"+ notifyElements.get(i).getMessage());

                txTime[i] = new TextView(this);
                txTime[i].setTextSize(15);
                txTime[i].setText("Time"+notifyElements.get(i).getTime());

                linear.addView(txGeofence[i]);
                linear.addView(txMessage[i]);
                linear.addView(txTime[i]);
            }

        }

        Button btnDelete = new Button(this);
        btnDelete.setText("Delete all reports");
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot ds: dataSnapshot.getChildren()){
                            ds.getRef().removeValue();

                        }
                        notifyElements.clear();
                        Toast.makeText(ViewReport.this, "Deleted", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(ViewReport.this, ViewReport.class);
                        startActivity(intent);


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("ERROR", "database error");
                    }
                });
            }
        });

        linear.addView(btnDelete);
    }
}
