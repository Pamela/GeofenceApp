package com.example.pamela.geofence;

/**
 * Created by pamela on 12/03/2018.
 */

public class NotifyElement {
    public String time;
    public String geofence;
    public String message;

    public NotifyElement(){}

    public NotifyElement(String geofence, String time, String message){
        this.time=time;
        this.geofence=geofence;
        this.message=message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGeofence() {
        return geofence;
    }

    public void setGeofence(String geofence) {
        this.geofence = geofence;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
