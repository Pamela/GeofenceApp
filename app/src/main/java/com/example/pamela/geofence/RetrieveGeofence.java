package com.example.pamela.geofence;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * Created by pamela on 17/02/2018.
 */

public class RetrieveGeofence extends AppCompatActivity {


    public static GeofenceElement ge;

    DatabaseReference ref;
    private int n;
    private ArrayList<GeofenceElement> geArray ;
    public static ArrayList<GeofenceElement> geSelectedArray;
    LinearLayout linear;
    Button btn1;
    CheckBox cb1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_geofence);
        geArray = new ArrayList<>();
        geSelectedArray = new ArrayList<>();
        linear = (LinearLayout) findViewById(R.id.linear);
        ref = FirebaseDatabase.getInstance().getReference("geofenceList");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                n = (int) dataSnapshot.getChildrenCount();
                for (DataSnapshot  ds: dataSnapshot.getChildren()){
                    GeofenceElement elementG = ds.getValue(GeofenceElement.class);
                     geArray.add(elementG);

                 }
                addCheckBox(n);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    private void addCheckBox(int n) {

        for (int i=0; i<n; i++){
            CheckBox cb = new CheckBox(this);
            cb.setId(i);

            cb.setText(geArray.get(i).getTitle());
            linear.addView(cb);
        }

        for (int i=0; i<n; i++){
            cb1 = (CheckBox)findViewById(i);
            final int finalI = i;
            cb1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checked = ((CheckBox) v).isChecked();
                    if(checked){
                        geSelectedArray.add(geArray.get(finalI));
                    }
                }
            });
        }

        Button btn = new Button(this);
        btn.setId(n+1);
        btn.setText("View geofence selected on map");
        linear.addView(btn);
        int btnid = btn.getId();
        btn1 = (Button)findViewById(btnid);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(geSelectedArray.size()>0){
                    Intent intent = new Intent(RetrieveGeofence.this,GeofenceMap.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(RetrieveGeofence.this, "no selected geofence", Toast.LENGTH_LONG).show();
                }

            }
        });

    }



}
