package com.example.pamela.geofence;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;

    //Play Service Location
    private static final int MY_PERMISSION_REQUEST_CODE=7192;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST= 300193;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    private static int UPDATE_INTERVAL = 5000;
    private static int FASTEST_INTERVAL = 9000;
    private static int DISPLACEMENT = 10;


    DatabaseReference ref;
    DatabaseReference ref1;
    GeoFire geoFire;

    Marker mCurrent;
    private Circle geofenceLimits;
    private LatLng dengerous_area;

    VerticalSeekBar mSeekBar;


    private int count;

    private String titleGeofence, radiusGeofence;
    private  float radiusGeofenceF;
    




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ref = FirebaseDatabase.getInstance().getReference("MyLocation");
        geoFire = new GeoFire(ref);

        mSeekBar = (VerticalSeekBar)findViewById(R.id.verticalSeekBar);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mMap.animateCamera(CameraUpdateFactory.zoomTo(progress),2000,null);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        FirebaseDatabase databaseReference = FirebaseDatabase.getInstance();
        ref1=databaseReference.getReference("geofenceList");
        ref1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                count = (int) dataSnapshot.getChildrenCount();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ERROR", "database error");
            }
        });

        Button btnSaveGeofence = (Button)findViewById(R.id.btnsave);
        btnSaveGeofence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveGeofence();
            }
        });
         Button btnViewGeofence = (Button)findViewById(R.id.btnview);
        btnViewGeofence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewGeofence();

            }
        });

        Button btnViewReport = (Button)findViewById(R.id.btnreport);
        btnViewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, ViewReport.class);
                startActivity(intent);

            }
        });


        setUpLocation();
    }




    private void viewGeofence() {
        Intent intent = new Intent(MapsActivity.this, RetrieveGeofence.class);
        startActivity(intent);
    }

    private void saveGeofence() {

        if(dengerous_area!=null) {

            String title = titleGeofence;
            double latitude = dengerous_area.latitude;
            double longitude = dengerous_area.longitude;
            boolean active = false;

            GeofenceElement ge = new GeofenceElement(latitude,longitude,title, radiusGeofence, active);

            ref1.push().setValue(ge);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    if (checkPlayService()){
                        buildGoogleApiClient();
                        createLocationRequest();
                        displayLocation();
                    }
                }
                break;
        }
    }

    private void setUpLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            //Request runtime permission
            ActivityCompat.requestPermissions(this,new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            },MY_PERMISSION_REQUEST_CODE );
        }else {
            if (checkPlayService()){
                buildGoogleApiClient();
                createLocationRequest();
                displayLocation();
            }
        }
    }

    private void displayLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation!=null){
            final double latitude = mLastLocation.getLatitude();
            final double longitude = mLastLocation.getLongitude();

            //Update to firebase
            geoFire.setLocation("You", new GeoLocation(latitude,longitude),
                    new GeoFire.CompletionListener() {
                        @Override
                        public void onComplete(String s, DatabaseError databaseError) {
                            //Add marker
                            if (mCurrent != null){
                                mCurrent.remove(); // remove old current
                            }
                            mCurrent = mMap.addMarker(new MarkerOptions()
                                                .position(new LatLng(latitude,longitude))
                                                .title("You"));

                            //Move camera to this position
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude),12.0f));
                        }
                    });



            Log.d("EDMTDEV", String.format("Your location was changed: %f / %f",latitude,longitude));

        }else {
            Log.d("EDMTDEV", "Can not get your location");
        }

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private boolean checkPlayService() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode!= ConnectionResult.SUCCESS)
        {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {
               GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else{
                Toast.makeText(this, "This divice is not supported",Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }




    @Override
    public void onMapReady(GoogleMap googleMap) {
       mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.setOnMapClickListener(this);

    }


    private void setDengerouArea(LatLng latLng, String titleGeofence, float radiusG) {
        //Create geofence
        double lat = latLng.latitude;
        double lon = latLng.longitude;
        dengerous_area = new LatLng(lat, lon);
        CircleOptions circleO = new CircleOptions()
                .center(dengerous_area)
                .radius(radiusG)
                .strokeColor(0x220000FF)
                .strokeWidth(5.0f);

         geofenceLimits=mMap.addCircle(circleO);

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdate();
    }

    private void startLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation= location;
        displayLocation();
    }


    @Override
    public void onMapClick(final LatLng latLng) {
        android.support.v7.app.AlertDialog.Builder myBuild = new android.support.v7.app.AlertDialog.Builder(MapsActivity.this);
        View addGeofenceView = getLayoutInflater().inflate(R.layout.add_geofence, null);
        myBuild.setView(addGeofenceView);
        final android.support.v7.app.AlertDialog adialog = myBuild.create();
        adialog.show();
        final EditText titleG = addGeofenceView.findViewById(R.id.titleg);
        final EditText radiusG = addGeofenceView.findViewById(R.id.radius);
        Button create = addGeofenceView.findViewById(R.id.create);

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleGeofence = titleG.getText().toString();
                radiusGeofence = radiusG.getText().toString();
                radiusGeofenceF = Float.parseFloat(radiusGeofence);
                adialog.dismiss();
                setDengerouArea(latLng,titleGeofence, radiusGeofenceF);

            }
        });

    }
}
