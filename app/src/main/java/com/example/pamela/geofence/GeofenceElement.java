package com.example.pamela.geofence;

/**
 * Created by pamela on 15/02/2018.
 */

public class GeofenceElement {

    public double latitude;
    public double longitude;
    public String title;
    public String radiusGeofence;
    public boolean active;
    public String id;


    public String getRadiusGeofence() {
        return radiusGeofence;
    }

    public void setRadiusGeofence(String radiusGeofence) {
        this.radiusGeofence = radiusGeofence;
    }

    public GeofenceElement(double latitude, double longitude, String title, String radiusGeofence, boolean active){
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.radiusGeofence = radiusGeofence;
        this.active = active;

    }

    public GeofenceElement(){}

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }



    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title=title;
    }

    public double getLatitude(){
        return latitude;
    }

    public void setLatitude(double latitude){
        this.latitude=latitude;
    }

    public double getLongitude(){
        return longitude;
    }

    public void setLongitude(double longitude){
        this.longitude=longitude;
    }


}
