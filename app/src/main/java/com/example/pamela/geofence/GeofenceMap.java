package com.example.pamela.geofence;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

/**
 * Created by pamela on 17/02/2018.
 */

public class GeofenceMap extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMapClickListener, ResultCallback<Status> {

    private GoogleMap mMap;

    //Play Service Location
    private static final int MY_PERMISSION_REQUEST_CODE=7192;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST= 300193;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;


    private static int UPDATE_INTERVAL = 5000;
    private static int FASTEST_INTERVAL = 9000;
    private static int DISPLACEMENT = 10;


    DatabaseReference ref;
    DatabaseReference ref2;
    DatabaseReference ref1;

    GeoFire geoFire;

    private Circle geofenceLimits[] =new Circle[RetrieveGeofence.geSelectedArray.size()];

    Marker mCurrent;


    private ArrayList<GeofenceElement> notificationSart = new ArrayList<>();
    private GeoQuery geoQuery[] = new GeoQuery[RetrieveGeofence.geSelectedArray.size()];


    VerticalSeekBar mSeekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.geofence_map_activity);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        notificationSart = RetrieveGeofence.geSelectedArray;
        ref = FirebaseDatabase.getInstance().getReference("MyLocation");
        ref2 = FirebaseDatabase.getInstance().getReference("geofenceList");
        geoFire = new GeoFire(ref);
        notificationSart = RetrieveGeofence.geSelectedArray;
        mSeekBar = (VerticalSeekBar) findViewById(R.id.verticalSeekBar);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mMap.animateCamera(CameraUpdateFactory.zoomTo(progress), 2000, null);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        FirebaseDatabase databaseReference = FirebaseDatabase.getInstance();
        ref1=databaseReference.getReference("notificationList");
        setUpLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_maps_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.back:
                back();
                return true;
            case R.id.startAllert:
                int checkstart = 0;
                for (int i = 0 ; i<notificationSart.size(); i++) {
                    if (!notificationSart.get(i).isActive()) {
                        checkstart=1;
                        notificationSart.get(i).setActive(true);
                        final String titleNotification = notificationSart.get(i).getTitle();
                        geoQuery[i] = geoFire.queryAtLocation(new GeoLocation(notificationSart.get(i).getLatitude(), notificationSart.get(i).getLongitude()), 0.5f);
                        geoQuery[i].addGeoQueryEventListener(new GeoQueryEventListener() {
                            @Override
                            public void onKeyEntered(String key, GeoLocation geoLocation) {
                                sendNotification(titleNotification, String.format("%s entered the dengeros area", key));
                            }

                            @Override
                            public void onKeyExited(String key) {
                                sendNotification(titleNotification, String.format("%s is not longer in the dengeros area", key));

                            }

                            @Override
                            public void onKeyMoved(String key, GeoLocation geoLocation) {
                                Log.d("MOVE", String.format("%s moved within the dengerous area [%f/%f]", key, geoLocation.latitude, geoLocation.longitude));
                            }

                            @Override
                            public void onGeoQueryReady() {

                            }

                            @Override
                            public void onGeoQueryError(DatabaseError databaseError) {
                                Log.e("ERROR", "" + databaseError);
                            }
                        });
                    }

                }
                if(checkstart==1) {
                    Toast.makeText(GeofenceMap.this, "Start notification", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(GeofenceMap.this, "notifications already activated", Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.stopAllert:
                int check = 0;
                for(int i = 0 ; i<notificationSart.size(); i++){
                    if(notificationSart.get(i).isActive()) {
                        notificationSart.get(i).setActive(false);
                        geoQuery[i].removeAllListeners();
                        check=1;
                    }

                }
                if(check==0){
                    Toast.makeText(GeofenceMap.this, "notifications are not active",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(GeofenceMap.this, "Stop notification",Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.report:
                viewReport();
                return true;
            case R.id.deleteGeofence:
               deleteGeofence();
                return true;
            case R.id.create:
                Intent intent =new Intent(GeofenceMap.this, MapsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deleteGeofence() {
        ref2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds: dataSnapshot.getChildren()){
                    for (int i = 0 ; i <notificationSart.size();i++){
                        String titleArray = notificationSart.get(i).getTitle();
                        String titleDs = ds.child("title").getValue().toString();
                        if (titleArray.equals(titleDs)){
                            if (notificationSart.get(i).isActive()){
                                geoQuery[i].removeAllListeners();
                            }
                            geofenceLimits[i].remove();
                            ds.getRef().removeValue();
                        }
                    }
                }
                Toast.makeText(GeofenceMap.this, "Deleted", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendNotification(String title, String content) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-HH:mm");
        NotifyElement notify = new NotifyElement(title,dateFormat.format(calendar.getTime()),content);
        ref1.push().setValue(notify);
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(content);
        NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(this,MapsActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(contentIntent);
        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        manager.notify(new Random().nextInt(),notification);
    }


    private void viewReport() {
        Intent intent = new Intent(GeofenceMap.this, ViewReport.class);
        startActivity(intent);
    }

    private void back() {
        Intent intent = new Intent(GeofenceMap.this, RetrieveGeofence.class);
        startActivity(intent);
    }

    private void setUpLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            //Request runtime permission
            ActivityCompat.requestPermissions(this,new String[]{
                    android.Manifest.permission.ACCESS_COARSE_LOCATION,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            },MY_PERMISSION_REQUEST_CODE );
        }else {
            if (checkPlayService()){
                buildGoogleApiClient();
                createLocationRequest();
                displayLocation();
            }
        }
    }

    private boolean checkPlayService() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode!= ConnectionResult.SUCCESS)
        {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
            {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else{
                Toast.makeText(this, "This divice is not supported",Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void displayLocation() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation!=null){
            final double latitude = mLastLocation.getLatitude();
            final double longitude = mLastLocation.getLongitude();

            //Update to firebase
            geoFire.setLocation("You", new GeoLocation(latitude,longitude),
                    new GeoFire.CompletionListener() {
                        @Override
                        public void onComplete(String s, DatabaseError databaseError) {
                            //Add marker
                            if (mCurrent != null){
                                mCurrent.remove(); // remove old current
                            }
                            mCurrent = mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(latitude,longitude))
                                    .title("You"));

                            //Move camera to this position
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude,longitude),12.0f));
                        }
                    });



            Log.d("EDMTDEV", String.format("Your location was changed: %f / %f",latitude,longitude));

        }else {
            Log.d("EDMTDEV", "Can not get your location");
        }

    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Log.d("LATITUDE", ""+ RetrieveGeofence.geSelectedArray.get(0).getLatitude()+" "+0);
        Log.d("LATITUDE", ""+ RetrieveGeofence.geSelectedArray.get(0).getLongitude()+" "+0);

        mMap.setMyLocationEnabled(true);
           drawGeoofence();


    }

    private void drawGeoofence() {
        for (int i = 0; i< RetrieveGeofence.geSelectedArray.size(); i++) {
            Log.d("LATITUDE", "" + RetrieveGeofence.geSelectedArray.get(i).getLatitude() + " " + i);
            Log.d("LATITUDE", "" + RetrieveGeofence.geSelectedArray.get(i).getLongitude() + " " + i);
            LatLng dengerous_area = new LatLng(RetrieveGeofence.geSelectedArray.get(i).getLatitude(), RetrieveGeofence.geSelectedArray.get(i).getLongitude());
            float radius = Float.parseFloat(RetrieveGeofence.geSelectedArray.get(i).radiusGeofence);
            CircleOptions circleO = new CircleOptions()
                    .center(dengerous_area)
                    .radius(radius)
                    .strokeColor(0x220000FF)
                    .strokeWidth(5.0f);

            geofenceLimits[i] = mMap.addCircle(circleO);
        }
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdate();


    }

    private void startLocationUpdate() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation= location;
        displayLocation();
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onResult(@NonNull Status status) {

    }
}
